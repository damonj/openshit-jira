from flask import Flask, request, jsonify
from jinja2 import Environment, FileSystemLoader
import json
import smtplib, ssl
from getpass import getpass
from email.message import EmailMessage
from base64 import b64decode
import os
import requests
import urllib3

application = Flask(__name__)
file_loader = FileSystemLoader('emailtemplates')
env = Environment(loader=file_loader)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

#Get Environment Variables
emailuser = os.environ.get('EMAIL_USER', None)
emailpass = os.environ.get('EMAIL_PASS', None)
smtpserver = os.environ.get('EMAIL_SERVER', None)
smtpport = os.environ.get('EMAIL_PORT', None)
jirauser = os.environ.get('JIRA_USER', None)
jirapass = os.environ.get('JIRA_PASS', None)
jiraurl = os.environ.get('JIRA_URL', None)
jirainprogress = os.environ.get('JIRA_INPROGRESS')

if None in [emailuser, emailpass, smtpserver, smtpport, jirauser, jirapass, jiraurl, jirainprogress]:
    print("Missing environent variables")
    exit()

def move_to_inprogress(jirauser, jirapass, baseurl, provid, jirainprogress):
    """
        Gimme a provid and I'll move it to in progress
    """
        
        
    print("Move Func : %s : %s : %s : %s" % (provid, jirainprogress, jirauser, jirapass))
    rest_url = baseurl + '/rest/api/latest/issue/' + provid +'/transitions'
    headers={'Content-type':'application/json', 'Accept':'application/json'}
    payload =  '{ "transition": { "id": "' + jirainprogress +'" } }'
    
    r = requests.post(url=rest_url, auth=(jirauser, jirapass), verify=False, data=str(payload), headers=headers)
    print("Move Result : %s" % dir(r))
    if r.status_code == 204:
        return True
    else:
        return False
        


def add_comment(jirauser, jirapass, baseurl, provid, comment):
    """
        Gimme a provid and I'll chuck a comment on it
    """
        
    rest_url =  baseurl + '/rest/api/latest/issue/' + provid +'/comment'
    headers={'Content-type':'application/json', 'Accept':'application/json'}
    payload =  '{ "body" : "'+ comment +'"}'
    
    r = requests.post(url=rest_url, auth=(jirauser, jirapass), verify=False, data=str(payload), headers=headers)
    if r.status_code == 201:
        return True
    else:
        return False


def send_email(subject, to, sendfrom, message):
    print("To : %s" % to)
    print("From : %s" % sendfrom)
    print("Subject : %s" % subject)

    msg = EmailMessage()

    header  = 'From: %s\n' % sendfrom
    header += 'To: %s\n' % to
    header += 'Subject: %s\n\n' % subject
    message = header + message


    server = smtplib.SMTP(smtpserver, smtpport)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(emailuser, emailpass)
    text = msg.as_string()
    server.sendmail(sendfrom, to, message)
    server.quit()
    return True


def check_fields(issue):
    """
        Given the issue object confirm that we have the required
        fields for our email

        return  True or False
    """
    try:
        IPADDRESSES = issue['fields']['customfield_10200']
        if IPADDRESSES is None:
            return False
    except:
        return False

    return True


@application.route('/', methods=['POST'])
def parse_request():
    content = request.get_json()
    changelog = content.get('changelog', None)
    if not changelog:
        return "OK"

    items = changelog['items']
    status_change = [ x for x in items if x['toString'] == 'Done' and x['field'] == 'status'] 
    print(status_change)
    issue = content.get('issue', False)
    if len(status_change) > 0:
      print("Issue Moved to : %s" % status_change[0]['toString'])
      if check_fields(issue):
        print("The required fields were found")
        template = env.get_template('completed.j2')
        content['name'] = 'Jacob'
        output = template.render(data=content)
        print("Sending success email")
        send_email('%s Completed Successfully' % content['issue']['key'], 'jacob@thedamons.uk','jacob@thedamons.uk', output)
        add_comment(jirauser, jirapass, jiraurl, content['issue']['key'], "Handover email sent by Jira Bot 3000")
      else:
        print("The required fields were NOT found")
        template = env.get_template('missingdetail.j2')
        content['name'] = 'Jacob'
        output = template.render(data=content)
        print("Sending Failed email")
        email_sent = send_email('%s Missing details' % content['issue']['key'], content['user']['emailAddress'], 'jacob@thedamons.uk', output)
        comment_added = add_comment(jirauser, jirapass, jiraurl, content['issue']['key'], "Jira Bot 3000 was not able to send the handover email becuase data was missing. Jira Bot 3000 moved the ticket back to in progres.")
        moved = move_to_inprogress(jirauser, jirapass, jiraurl, content['issue']['key'], jirainprogress)

      
    return 'JSON posted'
		
    
    
if __name__ == "__main__":
    application.run(host='0.0.0.0')
